import express from 'express';
import CategoryController from '../../controllers/category/category.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        CategoryController.validateBody(),
        CategoryController.create
    )
    .get(CategoryController.findAll);
    
router.route('/select-categories')
    .get(CategoryController.findSelection);

router.route('/:modelId/models')
    .get(CategoryController.findByModelId);
    
router.route('/:categoryId')
    .put(
        requireAuth,
        CategoryController.validateBody(true),
        CategoryController.update
    )
    .get(CategoryController.findById)
    .delete( requireAuth,CategoryController.delete);







export default router;