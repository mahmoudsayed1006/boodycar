
import express from 'express';
import BrandController from '../../controllers/brand/brand.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('brands').single('img'),
        BrandController.validateBody(),
        BrandController.create
    )
    .get(BrandController.findAll);
router.route('/topBrand')
    .get(BrandController.findTopBrand);
router.route('/:brandId')
    .put(
        requireAuth,
        multerSaveTo('brands').single('img'),
        BrandController.validateBody(true),
        BrandController.update
    )
    .get(BrandController.findById)
    .delete(requireAuth,BrandController.delete);

router.route('/:brandId/top')
    .put(
        requireAuth,
        BrandController.top
    )

router.route('/:brandId/low')
    .put(
        requireAuth,
        BrandController.low
    )




export default router;
