import express from 'express';
import SearchController from '../../controllers/search/search.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/products')
    .get(SearchController.smartSearch);

router.route('/normal')
    .post(
        SearchController.validateSearch(),
        SearchController.normalSearch
     );
//contact search
router.route('/')
    .post(requireAuth,SearchController.validateSearchCreateBody(), SearchController.createSearchMessage)
    .get(requireAuth,SearchController.findAll);

router.route('/guest')
    .post(SearchController.validateSearchCreateBody(), SearchController.createSearchMessageForGuest)
router.route('/:searchId')
    .get(SearchController.findById)
    .delete(requireAuth,SearchController.delete)

/*router.route('/:searchId/sendnotif')
    .get(SearchController.sendToOwners);
*/
router.route('/:searchId/exist')
    .put(requireAuth,SearchController.exist);

router.route('/:searchId/users/:userId/reply')
    .post(requireAuth,SearchController.validateSearchReplyBody(),SearchController.reply);

export default router;
