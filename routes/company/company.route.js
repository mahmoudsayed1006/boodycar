import express from 'express';
import CompanyController from '../../controllers/company/company.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('companies').single('img'),
        CompanyController.validateBody(),
        CompanyController.create
    )
    .get(CompanyController.findAll);

router.route('/topCompany')
    .get(CompanyController.findTopCompany);


router.route('/:companyId')
    .put(
        requireAuth,
        multerSaveTo('companies').single('img'),
        CompanyController.validateBody(true),
        CompanyController.update
    )
    .get(CompanyController.findById)
    .delete(requireAuth,CompanyController.delete);
  
router.route('/:companyId/top')
    .put(
        requireAuth,
        CompanyController.top
    )

router.route('/:companyId/low')
    .put(
        requireAuth,
        CompanyController.low
    )






export default router;
