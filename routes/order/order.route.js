import express from 'express';
import OrderController from '../../controllers/order/order.controller';
const router = express.Router();

router.route('/:userId/orders')
    .get(OrderController.findOwnerOrders)
    .post(
        OrderController.validateCreatedOrders(),
        OrderController.create
    );

router.route('/:userId/orders/:orderId')
    .get(OrderController.findById)
    .delete(OrderController.delete);
router.route('/:userId/orders/:orderId/status')
    .put(OrderController.validateThenUpdateOrderStatus())



export default router;
