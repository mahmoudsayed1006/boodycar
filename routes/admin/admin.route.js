import express from 'express';
import UserController from '../../controllers/user/user.controller';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/users')
    .get(adminController.findAll);

router.route('/count')
    .get(adminController.count);

router.route('/:userId/active')
.put(
   UserController.active
);
router.route('/:userId/dis-active')
.put(
   UserController.disactive
);




export default router;
