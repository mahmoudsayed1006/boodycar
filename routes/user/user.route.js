import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';
import CityController from '../../controllers/city/city.controller';
import AreaController from '../../controllers/area/area.controller';


const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );
router.route('/ownersignup')
.post(
    UserController.validateOwnerCreateBody(),
    UserController.signUpOwner
);


router.get('/cities/:cityId/area', AreaController.getAll);

router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/updateInfo',
    requireAuth,
    UserController.validateUpdatedBody(),
    UserController.updateInfo);
    
router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/forget-password',
    UserController.validateForgetPassword(),
    UserController.forgetPasswordSms);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);


export default router;
