import express from 'express';
import ModelsController from '../../controllers/models/models.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        ModelsController.validateBody(),
        ModelsController.create
    )
    .get(ModelsController.findAll);
    
router.route('/:companyId/companies')
    .get(ModelsController.findByCompany);

router.route('/select-model')
    .get(ModelsController.findSelection);

router.route('/:modelId')
    .put(
        requireAuth,
        ModelsController.validateBody(true),
        ModelsController.update
    )
    .get(ModelsController.findById)
    .delete( requireAuth,ModelsController.delete);







export default router;