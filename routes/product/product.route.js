import express from 'express';
import ProductController from '../../controllers/product/product.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('products').array('img', 6),
        ProductController.validateCreatedProduct(),
        ProductController.create
    );
//find all products
//router.route('/:userId/categories/:categoryId/products')
  //  .get(ProductController.findAll);

router.route('/categories/:categoryId/products')
    .get(ProductController.findAllInCategoy);

router.route('/companies/:companyId/products')
    .get(ProductController.findAllInCompany);

router.route('/')
    .get(ProductController.findAll);

router.route('/:ownerId/owner')
    .get(ProductController.findAllByOwner);
    
router.route('/topProduct')
    .get(ProductController.findTopProduct);

router.route('/:productId')
    .get(ProductController.findById)
    .put(
        requireAuth,
        multerSaveTo('products').array('img', 6),
        ProductController.validateCreatedProduct(true),
        ProductController.update
    )
    .delete(ProductController.delete);

router.route('/:productId/active')
    .put(
        requireAuth,
        ProductController.active
    )

router.route('/:productId/dis-active')
    .put(
        requireAuth,
        ProductController.disactive
    )

router.route('/:productId/top')
    .put(
        requireAuth,
        ProductController.top
    )

router.route('/:productId/low')
    .put(
        requireAuth,
        ProductController.low
    )


export default router;
