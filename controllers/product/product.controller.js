
import Product from "../../models/product/product.model";
import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import Company from "../../models/company/company.model";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { checkExistThenGet, checkExist, isArray } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import Model from "../../models/models/models.model";
import Year from "../../models/year/year.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import ApiError from '../../helpers/ApiError';

const populateQuery = [
    { path: 'category', model: 'category' },
    { path: 'company', model: 'Company' },
    { path: 'model', model: 'model' },
    { path: 'year', model: 'year' },
    { path: 'owner', model: 'user' }
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllByOwner(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let {ownerId} = req.params;
            let query = {deleted: false,owner:ownerId };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllInCategoy(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                , { categoryId } = req.params;
            await checkExist(categoryId, Category);
            let query = { category: +categoryId, visible: true, deleted: false };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const productCount = await Product.count(query);
            const pageCount = Math.ceil(productCount / limit);

            res.send(new ApiResponse(products, page, pageCount, limit, productCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllInCompany(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                , { companyId } = req.params;
            await checkExist(companyId, Company);
            let query = { company: +companyId, visible: true, deleted: false };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const productCount = await Product.count(query);
            const pageCount = Math.ceil(productCount / limit);

            res.send(new ApiResponse(products, page, pageCount, limit, productCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedProduct(isUpdate = false) {
        
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('status').not().isEmpty().withMessage('status is required'),
            body('price').not().isEmpty().withMessage('price is required')
                .isFloat({ min: 1 }).withMessage('price should be greater than 1'),
            body('quantity').not().isEmpty().withMessage('quantity is required'),
            body('company').not().isEmpty().withMessage('company is required')
            .isNumeric().withMessage('numeric value required'),
            body('year').not().isEmpty().withMessage('year is required')
            .isNumeric().withMessage('numeric value required'),
            body('category').not().isEmpty().withMessage('category is required')
            .isNumeric().withMessage('numeric value required'),
            body('model').not().isEmpty().withMessage('model is required')
            .isNumeric().withMessage('numeric value required'),
            
        ];
        return validations;
    },
    async create(req, res, next) {
        try {
            let userId = req.user._id;
           
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.company, Company, { deleted: false }),
            checkExist(validatedBody.category, Category, { deleted: false }),
            checkExist(validatedBody.year, Year, { deleted: false }),
            checkExist(userId, User, { deleted: false }),
            checkExist(validatedBody.model, Model, { deleted: false });

            let img = await handleImgs(req);
            let product = await Product.create({
                ...validatedBody,
                img: img, 
                owner: userId
            });
            let reports = {
                "action":"Create New Product",
            };
            let report = await Report.create({...reports, user: req.user });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: product.id,
                    subjectType: 'user add new product'
                });
                let notif = {
                    "description":'new product'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:product.id});
            });
            res.status(201).send(product);
            
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { productId } = req.params;

            await checkExist(productId, Product,
                { deleted: false });
            let product = await Product.findById(productId).populate(populateQuery);
            res.send(
                product
            );
        } catch (err) {
            next(err);
        }
    },

    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.visible = true;
            await product.save();
            let reports = {
                "action":"Active Product",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'boody car accept your Product'
            });
            let notif = {
                "description":'boody car accept your Product'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.visible = false;
            await product.save();
            let reports = {
                "action":"Dis-active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product dis-active');
        } catch (error) {
            next(error);
        }
    },
    async findTopProduct(req, res, next){
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { top: true,deleted: false };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    
    async top(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            
            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.top = true;
            await product.save();
            let reports = {
                "action":"Make Product Top",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'boody car Make your Product Top'
            });
            let notif = {
                "description":'boody car Make your Product Top'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product been top');
           
        } catch (error) {
            next(error);
        }
    },

    async low(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            
            let { productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.top = false;
            await product.save();
            let reports = {
                "action":"Make Product Low",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'boody car Make your Product Low'
            });
            let notif = {
                "description":'boody car Make your Product Low'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product been low');
 
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            
            let {productId } = req.params;
            await checkExist(productId, Product,
                {deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                validatedBody.img = await handleImgs(req, { isUpdate: true, attributeName: 'img' });
            }

            let updatedProduct = await Product.findByIdAndUpdate(productId, {
                ...validatedBody,

            }, { new: true }).populate(populateQuery);

            let reports = {
                "action":"Update Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedProduct);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let {productId } = req.params;

            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.deleted = true
            await product.save();
            let reports = {
                "action":"Delete Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    }
}