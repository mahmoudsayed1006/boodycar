import ApiResponse from "../../helpers/ApiResponse";
import Year from "../../models/year/year.model";
import User from "../../models/user/user.model";
import Model from "../../models/models/models.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let years = await Year.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const yearsCount = await Year.count(query);
            const pageCount = Math.ceil(yearsCount / limit);

            res.send(new ApiResponse(years, page, pageCount, limit, yearsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('year').not().isEmpty().withMessage('year is required')
            .isNumeric().withMessage('number value require')
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdYear = await Year.create({ ...validatedBody});

            let reports = {
                "action":"Create Year",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdYear);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { yearId } = req.params;
            await checkExist(yearId, Year, { deleted: false });
            let year = await Year.findById(cyearId);
            res.send(year);
        } catch (err) {
            next(err);
        }
    },
    async findByModelId(req, res, next) {
        try {
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });
            let query = {
                $and: [
                    {_id:modelId}, 
                    {deleted: false} 
                ]
            }
            let yearIds = await Model.find(query).distinct('year');
            console.log(yearIds);
            let year = await Year.find({_id:yearIds});
            res.send(year);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { yearId } = req.params;
            await checkExist(yearId, Year, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedYear = await Year.findByIdAndUpdate(yearId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Year",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedYear);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { yearId } = req.params;
            let year = await checkExistThenGet(yearId, Year, { deleted: false });
            year.deleted = true;
            await year.save();
            let reports = {
                "action":"Delete Year",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};