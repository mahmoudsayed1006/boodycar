import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Company from '../../models/company/company.model';
import ApiError from '../../helpers/ApiError';
import bcrypt from 'bcryptjs';
import config from '../../config';
import { sendForgetPassword } from '../../services/message-service';
import { generateVerifyCode } from '../../services/generator-code-service';
import City from '../../models/city/city.model';
import Area from '../../models/area/area.model';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}

const populateQuery = [
    { path: 'company', model: 'Company' },
    { path: 'area', model: 'area' },
    { path: 'city', model: 'city' }
];
export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let user = req.user;
           
            user = await User.findById(user.id).populate(populateQuery),
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
            
            let reports = {
                "action":"User Login",
            };

            let report = await Report.create({...reports, user: user });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('firstname').not().isEmpty().withMessage('firstname is required'),
            body('lastname').not().isEmpty().withMessage('lastname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT', 'OWNER', 'ADMIN']).withMessage('wrong type'),
            body('area').not().isEmpty().withMessage('area is required')
                .isNumeric().withMessage('numeric value required'), 
            body('city').not().isEmpty().withMessage('city is required')
                .isNumeric().withMessage('numeric value required'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    validateOwnerCreateBody(isUpdate = false) {
        let validation = [
            body('firstname').not().isEmpty().withMessage('firstname is required'),
            body('lastname').not().isEmpty().withMessage('lastname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT', 'OWNER', 'ADMIN']).withMessage('wrong type'),
            body('area').not().isEmpty().withMessage('area is required')
                .isNumeric().withMessage('numeric value required'), 
            body('city').not().isEmpty().withMessage('city is required')
                .isNumeric().withMessage('numeric value required'),
            body('location').not().isEmpty().withMessage('location is required')
                .isNumeric().withMessage('numeric value required'),
            body('company').not().isEmpty().withMessage('company is required')
                .isNumeric().withMessage('numeric value required'),
        ];
        if (!isUpdate) {
            validation.push([
                body('password').not().isEmpty().withMessage('password is required')
            ])
        }
        return validation;
    },


    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.city, City, { deleted: false }),
            checkExist(validatedBody.area, Area, { deleted: false }),
            checkExist(validatedBody.company, Company, { deleted: false });
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            
            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    async signUpOwner(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.area, Area, { deleted: false }),
                  checkExist(validatedBody.city, City, { deleted: false }),
                  checkExist(validatedBody.company, Company, { deleted: false });
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });

            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            let reports = {
                "action":"Owner Sign Up",
            };
            let report = await Report.create({...reports,user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = true;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'boody car accept your register'
            });
            let notif = {
                "description":'boody car accept your rigister'
            }
            await Notif.create({...notif,resource:req.user,target:userId,subject:activeUser.id});
            res.send('owner active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            let reports = {
                "action":"Dis-Active Owner",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id).populate({
                path: 'area',
                populate: {
                    path: 'city'
                }
            });
            res.send(user);
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },
    validateUpdatedBody(isUpdate = true) {
        let validation = [
            body('firstname').not().isEmpty().withMessage('firstname is required'),
            body('lastname').not().isEmpty().withMessage('lastname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    userQuery._id = { $ne: req.user._id };
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('area').not().isEmpty().withMessage('area is required')
                .isNumeric().withMessage('numeric value required'), 
            body('city').not().isEmpty().withMessage('city is required')
                .isNumeric().withMessage('numeric value required'),
            body('company').optional().isNumeric().withMessage('numeric value required'),
            body('location').optional().isNumeric().withMessage('numeric value required'),
        ];

        return validation;
    },
    async updateInfo(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(req.user._id, User);
            user.phone = validatedBody.phone;
            user.firstname = validatedBody.firstname;
            user.lastname = validatedBody.lastname;
            user.email = validatedBody.email;
            user.area = validatedBody.area;
            user.city = validatedBody.city;
            if(validatedBody.company){
                user.company = validatedBody.company;
            }
            if(validatedBody.location){
                user.location = validatedBody.company;
            }
           
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send({
                user: await User.findById(req.user._id).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    },

    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    validateForgetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required')
        ];
    },
    async forgetPasswordSms(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let realPhone = config.countrykey + validatedBody.phone;
            let user = await checkUserExistByPhone(validatedBody.phone);

            user.verifycode = generateVerifyCode();
            await user.save();
            //send sms
            sendForgetPassword(user.verifycode, realPhone);
            let reports = {
                "action":"send verify code to user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required'),
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
            let reports = {
                "action":"User reset Password",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },
    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    }


};
