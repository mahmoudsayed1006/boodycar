import ApiResponse from "../../helpers/ApiResponse";
import Company from "../../models/company/company.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Product from "../../models/product/product.model";
import ApiError from '../../helpers/ApiError';


export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let companies = await Company.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const companiesCount = await Company.count(query);
            const pageCount = Math.ceil(companiesCount / limit);

            res.send(new ApiResponse(companies, page, pageCount, limit, companiesCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('companyname').not().isEmpty().withMessage('companyname is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.companyId };

                    let company = await Company.findOne(query).lean();
                    if (company)
                        throw new Error('company duplicated name');

                    return true;
                })
        ];
        if (isUpdate)
            validations.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let createdCompany = await Company.create({ ...validatedBody, img: image });

            let reports = {
                "action":"Create Company",
            };
            let report = await Report.create({...reports, user: user });

            res.status(201).send(createdCompany);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { companyId } = req.params;
            await checkExist(companyId, Company, { deleted: false });
            let company = await Company.findById(companyId);
            res.send(company);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { companyId } = req.params;
            await checkExist(companyId, Company, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validateBody.img = image;
            }

            let updatedCompany = await Company.findByIdAndUpdate(companyId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Company",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedCompany);
        }
        catch (err) {
            next(err);
        }
    },
    
    async top(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { companyId} = req.params;
            let company = await checkExistThenGet(companyId, Company);
            company.top = true;
            await company.save();
            let reports = {
                "action":"Make Company Top",
            };
            let report = await Report.create({...reports, user: user });
            res.send('company been top');
        } catch (error) {
            next(error);
        }
    },

    async low(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { companyId } = req.params;
            let company = await checkExistThenGet(companyId, Company);

            company.top = false;
            await company.save();
            let reports = {
                "action":"Make Company Low",
            };
            let report = await Report.create({...reports, user: user });
            res.send('company been low');
        } catch (error) {
            next(error);
        }
    },
    async findTopCompany(req,res, next){
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false,top:true };
            let companies = await Company.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const companiesCount = await Company.count(query);
            const pageCount = Math.ceil(companiesCount / limit);

            res.send(new ApiResponse(companies, page, pageCount, limit, companiesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { companyId } = req.params;

            let company = await checkExistThenGet(companyId, Company, { deleted: false });
            let products = await Product.find({ company: companyId });
            for (let product of products ) {
                product.deleted = true;
                await product.save();
            }
            company.deleted = true;
            await company.save();
            let reports = {
                "action":"Delete Company",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },

    
};