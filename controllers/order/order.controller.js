import { checkExist, checkExistThenGet, isLng, isLat, isArray, isNumeric } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Order from "../../models/order/order.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import moment from 'moment';
import Product from "../../models/product/product.model";
import { ValidationError } from "mongoose";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model"
import { config } from "cloudinary";

const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'owner', model: 'user' },
    {
        path: 'productOrders.product', model: 'product',
        populate: { path: 'category', model: 'category' }
    }
];
function validatedestination(location) {
    if (!isLng(location[0]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[0] is invalid lng' });
    if (!isLat(location[1]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[1] is invalid lat' });
}
function checkMyAuthorityToUpdateTheOrder(currentUser, statusInBody, order) {
    switch (order.status) {
        case 'PENDING':
            if (statusInBody !== 'ACCEPTED' && statusInBody !== 'REFUSED')
                throw new ApiError(400, 'status should be "ACCEPTED" OR "REFUSED"');
            if (order.owner !== currentUser._id)
                throw new ApiError.Forbidden('Order\'s owner is the only one who can accept, refuse or finish the order');
            break;
        case 'ACCEPTED':
            if (statusInBody !== 'FINISHED')
                throw new ApiError(400, 'status should be "FINISHED"');
            if (order.owner !== currentUser._id)
                throw new ApiError.Forbidden('Order\'s owner is the only one who can accept, refuse or finish the order');
            break;
        case 'FINISHED':
            if (statusInBody !== 'DELIVERED')
                throw new ApiError(400, 'status should be "DELIVERED"');
            // check for client only in case it's DELIVERY_GUY so it will be set by deliveryGuyOrder status ontheway
            if (+order.client.id !== currentUser._id)
                throw new ApiError.Forbidden('Order\'s Client is the only one who can confirm delivered the order');
            break;
        case 'REFUSED':
        case 'DELIVERED':
            throw new ApiError(400, 'you can\'t update order which is REFUSED or DELIVERED');
    }
}
export default {
    async findOwnerOrders(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                , { userId } = req.params, { status } = req.query
                , query = { owner: userId, deleted: false };

            if (status)
                query.status = { $in: status };

            await checkExist(userId, User);

            let orders = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);

            res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedOrders() {
        let validations = [
            body('destination').not().isEmpty().withMessage('destination is required'),
            body('productOrders').custom(vals => isArray(vals)).withMessage('productOrders should be an array')
                .isLength({ min: 1 }).withMessage('productOrders should have at least one element of productOrder')
                .custom(async (productOrders, { req }) => {
                    let prevProductId;
                    for (let productOrder of productOrders) {
                        // check if it's duplicated product
                        if (prevProductId && prevProductId === productOrder.product)
                            throw new Error(`Duplicated Product : ${productOrder.product}`);
                        prevProductId = productOrder.product;
                        
                        // check if count is a valid number 
                        if (!isNumeric(productOrder.count))
                            throw new Error(`Product: ${productOrder.product} has invalid count: ${productOrder.count}!`);
                    }
                    return true;
                }),
        ];
        return validations;
    },
            
    async create(req, res, next) {
        let { userId } = req.params; 
        try {
            await checkExist(userId, User);
            await checkExist(req.user._id, User);
            const validatedBody = checkValidations(req);
            validatedBody.client = req.user._id;
            validatedBody.owner = userId;
            validatedestination(validatedBody.destination);
            validatedBody.destination = { type: 'Point', coordinates: [+req.body.destination[0], +req.body.destination[1]] };
            let total = 0;
            for (let singleProduct of validatedBody.productOrders) {
                let productDetail = await checkExistThenGet(singleProduct.product, Product);
                    total += productDetail.price * singleProduct.count;
            }
            validatedBody.total = total;
            let createdOrder = await Order.create({ ...validatedBody });
            let order = await Order.populate(createdOrder, populateQuery);
            let reports = {
                "action":"Create New Order",
            };
            let report = await Report.create({...reports, user: req.user });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: validatedBody.client, 
                    text: 'new notification',
                    subject: createdOrder.id,
                    subjectType: 'new order'
                });
                let notif = {
                    "description":'new order'
                }
                Notif.create({...notif,resource:validatedBody.client,target:user.id,subject:createdOrder.id});
            });
            
            res.status(201).send(order);
        } catch (err) {
            next(err);
            
        }
    },

    async findById(req, res, next) {
        try {
            let { userId, orderId } = req.params;
            await (userId, User);
            res.send(
                await checkExistThenGet(orderId, Order, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    validateThenUpdateOrderStatus() {
        let validations = [
            body('status').not().isEmpty().withMessage('status is required')
                .isIn(['ACCEPTED', 'REFUSED', 'FINISHED', 'DELIVERED']).withMessage('status should be one of ACCEPTED, REFUSED, FINISHED, DELIVERED')
        ];
        return [validations, this.changeOrderStatus];
    },
    async changeOrderStatus(req, res, next) {
        try {
            let { userId, orderId } = req.params;
            await (userId, User);
            let order = await checkExistThenGet(orderId, Order, { deleted: false });
            const validatedBody = checkValidations(req);
            let statusInBody = validatedBody.status;
            checkMyAuthorityToUpdateTheOrder(req.user, statusInBody, order);
            await Order.findByIdAndUpdate(orderId, { status: statusInBody });
            let reports = {
                "action":"Change Order Status",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            if (['ACCEPTED', 'REFUSED', 'FINISHED', 'DELIVERED'].includes(order.status))
                next(ApiError(400, 'status should be "PENDING" to delete this order'));
            order.deleted = true;
            await order.save();
            let reports = {
                "action":"Delete Order",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
}