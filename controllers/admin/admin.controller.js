import ApiResponse from "../../helpers/ApiResponse";
import Model from "../../models/models/models.model";
import Company from "../../models/company/company.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Order from "../../models/order/order.model";
import Product from "../../models/product/product.model";
import Search from "../../models/search/search.model";

export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
                { accepted, type, name } = req.query;
            let query = { deleted: false };

            if (accepted || accepted == 'true') query.accepted = true
            if (accepted == 'false') query.accepted = false
            if (type) query.type = type
            if (name) query.name = new RegExp(escapeRegExp(req.query.name), 'i')

            let users = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const userCount = await User.count(query);
            const pageCount = Math.ceil(userCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, userCount, req));
        } catch (err) {
            next(err);
        }
    },
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const modelsCount = await Model.count(query);
            const usersCount = await User.count(query);
            const companyCount = await Company.count(query);
            const ordersCount = await Order.count(query);
            const searchCount = await Search.count(query);
            const productsCount = await Product.count(query);
            const reportsCount = await Report.count(query);
            
            res.status(200).send({
                modelsCount,
                usersCount,
                companyCount,
                ordersCount,
                searchCount,
                productsCount,
                reportsCount,
                
            });
        } catch (err) {
            next(err);
        }
        
    },

}