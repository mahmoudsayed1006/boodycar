
import Product from "../../models/product/product.model";
import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import Company from "../../models/company/company.model";
import Search from "../../models/search/search.model";
import { checkValidations } from "../shared/shared.controller";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import Model from "../../models/models/models.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import ApiError from '../../helpers/ApiError';

const populateQuery = [
    { path: 'category', model: 'category' },
    { path: 'company', model: 'Company' },
    { path: 'model', model: 'model' },
    { path: 'owner', model: 'user' },
    { path: 'availableProduct', model: 'product' }
];

export default {
    async smartSearch (req,res,next){
        try{

            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { categoryId, modelId, companyId} = req.query;


            let query = { deleted: false ,visible: true };
            if (categoryId) query.category = categoryId;
            if (modelId) query.model = modelId;
            if (companyId) query.company = companyId;
            
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const productCount = await Product.count(query);
            const pageCount = Math.ceil(productCount / limit);
            let reports = {
                "action":"Search About Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send(new ApiResponse(products, page, pageCount, limit, productCount, req));
        }
        catch(err){
            next(err);
        }
    },
    validateSearch() {
        return [
            body('search').not().isEmpty().withMessage('search message required')
        ]
    }, 
    async normalSearch (req,res,next){
        try{
            const validatedBody = checkValidations(req);
            let page = +req.query.page || 1, limit = +req.query.limit || 20
            let query = {
                $and: [
                    {name: new RegExp('^'+validatedBody.search+'$', "i")},
                    {visible: true}, 
                    {deleted: false} 
                ]
            };
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const productCount = await Product.count(query);
            const pageCount = Math.ceil(productCount / limit);

            let reports = {
                "action":"Search About Brand",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send(new ApiResponse(products, page, pageCount, limit, productCount, req));
        }
        catch(err){
            next(err);
        }
    },
    validateSearchCreateBody() {
        return [
            body('search').not().isEmpty().withMessage('search message required'),
            body('phone')
        ]
    },
    async createSearchMessage(req, res, next) {
        try {
            let userId = req.user._id;
            const validatedBody = checkValidations(req);
            validatedBody.user = userId;
            let search = await Search.create({ ...validatedBody });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'new search message'
                });
                let notif = {
                    "description":'new search message'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:search.id});
            });
            let owners = await User.find({'type':'OWNER'});
            owners.forEach(owner => {
                sendNotifiAndPushNotifi({////////
                    targetUser: owner.id, 
                    fromUser: 'boody car', 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'someone search about this product'
                });
                let notif = {
                    "description":'someone search about this product'
                }
                Notif.create({...notif,target:owner.id,subject:search.id});
            });
            let reports = {
                "action":"Search Message ",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send('message send');
        } catch (error) {
            next(error);
        }
    },
    async createSearchMessageForGuest(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let search = await Search.create({ ...validatedBody });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser:'guest', 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'new search message'
                });
                let notif = {
                    "description":'new search message'
                }
                Notif.create({...notif,target:user.id,subject:search.id});
            });
            let owners = await User.find({'type':'OWNER'});
            owners.forEach(owner => {
                sendNotifiAndPushNotifi({////////
                    targetUser: owner.id, 
                    fromUser: 'boody car', 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'someone search about this product'
                });
                let notif = {
                    "description":'someone search about this product'
                }
                Notif.create({...notif,target:owner.id,subject:search.id});
            });
            let reports = {
                "action":"Search Message ",
            };
            let report = await Report.create({...reports});
            res.status(204).send('message send');
        } catch (error) {
            next(error);
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20, query = { deleted: false };

            await checkExist(req.user._id, User);
            let user = req.user;
            if (user.type !== 'ADMIN')
                return next(new ApiError(403, ('admin auth')));

            let searchs = await Search.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const searchsCount = await Search.count(query);
            const pageCount = Math.ceil(searchsCount / limit);

            res.send(new ApiResponse(searchs, page, pageCount, limit, searchsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { searchId } = req.params;
            res.send(
                await checkExistThenGet(searchId, Search, { populate: 'user' })
            );
        } catch (err) {
            next(err);
        }
    },
 
    /*async sendToOwners(req,res,next){
        try{
            let { searchId } = req.params;
            let search = await checkExistThenGet(searchId, Search);
            let users = await User.find({'type':'OWNER'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'someone search about this product'
                });
                let notif = {
                    "description":'someone search about this product'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:search.id});
            });
            res.status(201).send('notifacation send');
        } catch(err){
            next(err)
        }
    },*/
    async exist(req,res,next){
        try{
            let { searchId } = req.params;
            let search = await checkExistThenGet(searchId, Search);
            search.exist = true;
            search.availableProduct =req.body.availableProduct;
            await search.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: search.id,
                    subjectType: 'product exist'
                });
                let notif = {
                    "description":'product exist'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:search.id});
            });
            res.status(201).send('notifacation send');
        } catch(err){
            next(err)
        }
    },
    validateSearchReplyBody() {
        let validation = [
            body('reply').not().isEmpty().withMessage('reply required')
        ]
        return validation;
    },
    async reply(req, res, next) {
        let { userId,searchId } = req.params; 
        try {
            let search = await checkExistThenGet(searchId, Search);
            let product = await Product.find({name:search.search});
            if(product){
                const validatedBody = checkValidations(req);
                search.reply =  validatedBody.reply;
                await search.save();
                let reports = {
                    "action":"Reply",
                };
                let report = await Report.create({...reports, user: req.user }); 
                sendNotifiAndPushNotifi({
                    targetUser: userId, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: validatedBody.reply,
                    subjectType: 'boody car reply on your search order'
                });
                let notif = {
                    "description":'boody car reply on your search order'
                }
                await Notif.create({...notif,resource:req.user,target:userId,subject:search.id});
                res.status(204).send('replay send');
            } else{
                res.status(204).send('the owner didnot insert the product')
            }
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { searchId } = req.params;
            let search = await checkExistThenGet(searchId, Search);
            search.deleted = true;
            await search.save();
            let reports = {
                "action":"Delete Message Search",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send('delete success');
        } catch (err) {
            next(err);
        }
    },
}