import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Model from "../../models/models/models.model";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Product from "../../models/product/product.model";
import ApiError from '../../helpers/ApiError';

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let categories = await Category.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const categoriesCount = await Category.count(query);
            const pageCount = Math.ceil(categoriesCount / limit);

            res.send(new ApiResponse(categories, page, pageCount, limit, categoriesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let categories = await Category.find(query)
                .sort({ createdAt: -1 });
            res.send(categories)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('categoryname').not().isEmpty().withMessage('categoryname is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.categoryId };

                    let category = await Category.findOne(query).lean();
                    if (category)
                        throw new Error('category duplicated name');

                    return true;
                })
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdCategory = await Category.create({ ...validatedBody});

            let reports = {
                "action":"Create Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdCategory);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { categoryId } = req.params;
            await checkExist(categoryId, Category, { deleted: false });
            let category = await Category.findById(categoryId);
            res.send(category);
        } catch (err) {
            next(err);
        }
    },

    async findByModelId(req, res, next) {
        try {
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });
            let query = {
                $and: [
                    {_id:modelId}, 
                    {deleted: false} 
                ]
            }
            let categoryIds = await Model.find(query).distinct('category');
            let category = await Category.find({_id:categoryIds});
            res.send(category);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { categoryId } = req.params;
            await checkExist(categoryId, Category, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedCategory = await Category.findByIdAndUpdate(categoryId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedCategory);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { categoryId } = req.params;
            let category = await checkExistThenGet(categoryId, Category, { deleted: false });
            let products = await Product.find({ company: companyId });
            for (let product of products ) {
                product.deleted = true;
                await product.save();
            }
            category.deleted = true;
            await category.save();
            let reports = {
                "action":"Delete Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};