import City from "../../models/city/city.model";
import Area from "../../models/area/area.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";

export default {
    validateCityBody(isUpdate = false) {
        return [
            body('cityName').not().isEmpty().withMessage('cityName Required')
                .custom(async (value, { req }) => {
                    let userQuery = { cityName: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.cityId };
                    if (await City.findOne(userQuery))
                        throw new Error('cityName duplicated');
                    else
                        return true;
                })
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            let city = await City.create({ ...validatedBody });
            let reports = {
                "action":"Create City",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(city);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
           
            let cities = await City.find({ deleted: false });

            return res.status(200).send(cities);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));

            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let cities = await City.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await City.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(cities, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { cityId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let city = await City.findByIdAndUpdate(cityId, { ...validatedBody });
            let reports = {
                "action":"Update City",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { cityId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let city = await checkExistThenGet(cityId, City, { deleted: false });
            return res.send(city);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { cityId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let city = await checkExistThenGet(cityId, City, { deleted: false });
            let areas = await Area.find({ city: cityId });
            for (let areaId of areas) {
                areaId.deleted = true;
                await areaId.save();
            }
            city.deleted = true;
            await city.save();
            let reports = {
                "action":"Delete City",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
}