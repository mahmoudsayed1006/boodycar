import ApiResponse from "../../helpers/ApiResponse";
import Model from "../../models/models/models.model";
import Company from "../../models/company/company.model";
import Category from "../../models/category/category.model";
import Year from "../../models/year/year.model";
import ApiError from '../../helpers/ApiError';

import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Product from "../../models/product/product.model";


const populateQuery = [
    { path: 'company', model: 'Company' },
    { path: 'category', model: 'category' },
    { path: 'year', model: 'year' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            let query = { deleted: false };
            let models = await Model.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const modelsCount = await Model.count(query);
            const pageCount = Math.ceil(modelsCount / limit);

            res.send(new ApiResponse(models, page, pageCount, limit, modelsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let models = await Model.find(query)
                .sort({ createdAt: -1 });
            res.send(models)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('company').not().isEmpty().withMessage('company is required')
            .isNumeric().withMessage('numeric value required'),
            body('year').not().isEmpty().withMessage('year is required')
            .isNumeric().withMessage('numeric value required'),
            body('category').not().isEmpty().withMessage('category is required')
            .isNumeric().withMessage('numeric value required'),
            body('modelname').not().isEmpty().withMessage('model name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.modelId };

                    let company = await Company.findOne(query).lean();
                    if (company)
                        throw new Error('model duplicated name');

                    return true;
                })
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.company, Company, { deleted: false });
            let createdModel = await Model.create({
                ...validatedBody,
            });
            let reports = {
                "action":"Create New Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdModel);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });
            let model = await Model.findById(modelId).populate(populateQuery);
            res.send(model);
        } catch (err) {
            next(err);
        }
    },
    async findByCompany(req, res, next) {
        try {
            let { companyId } = req.params;
            await checkExist(companyId, Company, { deleted: false });
            let query = {
                $and: [
                    {company:companyId}, 
                    {deleted: false} 
                ]
            }
            let model = await Model.find(query).populate(populateQuery);
            res.send(model);
        } catch (err) {
            next(err);
        }
    },
    

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedModel = await Model.findByIdAndUpdate(modelId, {
                ...validatedBody,
            }, { new: true }).populate(populateQuery);
            let reports = {
                "action":"Update Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedModel);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { modelId } = req.params;

            let model = await checkExistThenGet(modelId, Model, { deleted: false });
            let products = await Product.find({ company: companyId });
            for (let product of products ) {
                product.deleted = true;
                await product.save();
            }
            model.deleted = true;

            await model.save();
            let reports = {
                "action":"Delete Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('item deleted');

        }
        catch (err) {
            next(err);
        }
    },

};