import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const OrderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    client: {
        type: Number,
        ref: 'user'
    },
    owner: {
        type: Number,
        ref: 'user'
    },
    total: {
        type: Number,
        required: true
    },
    destination: {
        type: { type: String, enum: 'Point' },
        coordinates: { type: [Number] }
    },
    status: {
        type: String,
        enum: ['PENDING', 'ACCEPTED', 'REFUSED', 'FINISHED', 'DELIVERED'],
        default: 'PENDING'
    },
    productOrders: [
        new Schema({
            product: {
                type: Number,
                ref: 'product',
                required: true
            },
            count: {
                type: Number,
                default: 1
            }
        }, { _id: false })
    ],
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OrderSchema.index({ location: '2dsphere' });
OrderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
    }
});
autoIncrement.initialize(mongoose.connection);
OrderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });

export default mongoose.model('order', OrderSchema);