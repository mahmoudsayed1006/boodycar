import mongoose,{ Schema} from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const CompanySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    companyname: {
        type: String,
        trim: true,
        required: true,
    },
    img: {
        type: String,
        required: true,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    top: {
        type: Boolean,
        default: false
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

CompanySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CompanySchema.plugin(autoIncrement.plugin, { model: 'Company', startAt: 1 });

export default mongoose.model('Company', CompanySchema);