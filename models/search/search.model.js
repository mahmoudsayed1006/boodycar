import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const SearchSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    search: {
        type: String,
        trim: true,
        required: true,
    },
    user:{
        type:Number,
        ref:'user'
    },
    reply:{
        type:String
    },
    exist:{
        type:Boolean,
        default:false
    },
    availableProduct:{
        type:Number,
        ref:'product'
    },
    phone:{
        type:String
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

SearchSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
SearchSchema.plugin(autoIncrement.plugin, { model: 'search', startAt: 1 });
export default mongoose.model('search', SearchSchema);