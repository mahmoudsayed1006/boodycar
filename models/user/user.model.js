import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        validate: {
            validator: (email) => isEmail(email),
            message: 'Invalid Email Syntax'
        }

    },
    phone: {
        type: String,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true
    },
    area: {
        type: Number,
        ref: 'area'
    },
    city: {
        type: Number,
        ref: 'city'
    },
    type: {
        type: String,
        enum: ['CLIENT', 'OWNER', 'ADMIN','SUB-ADMIN'],
        required:true
    },
    location: {
        type: [Number]
    },
    company: {
        type: [Number],
        ref:'company'
    },
    active:{
        type: Boolean,
        default: false
    },
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);