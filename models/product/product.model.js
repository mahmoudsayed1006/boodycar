import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const ProductSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity:{
        type:Number,
        required:true
    },
    category: {
        type: [Number],
        ref: 'category'
    },
    status:{
        type:String,
        required:true
    },
    company:{
        type:Number,
        ref:'company',
        required:true
    },
    img: {
        type: String,
        required: true,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    model:{
        type:Number,
        ref:'model',
        required:true
    },
    owner:{
        type:Number,
        ref:'user',
        required:true
    },
    year:{
        type:Number,
        ref:'year',
        required:true

    },
    visible: {
        type: Boolean,
        default: false
    },
    top: {
        type: Boolean,
        default: false
    },
    deleted:{
        type:Boolean,
        default:false
    },

});
ProductSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ProductSchema.plugin(autoIncrement.plugin, { model: 'product', startAt: 1 });

export default mongoose.model('product', ProductSchema);