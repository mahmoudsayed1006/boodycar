import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const ModelSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    modelname: {
        type: String,
        trim: true,
        required: true,
    },
    company:{
        type:Number,
        ref:'company'
    },
    category:{
        type:[Number],
        ref:'category'
    },
    year:{
        type:[Number],
        ref:'year'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

ModelSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ModelSchema.plugin(autoIncrement.plugin, { model: 'model', startAt: 1 });

export default mongoose.model('model', ModelSchema);